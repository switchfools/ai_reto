#!/usr/bin/env python
import rospy
import numpy as np 
import random
import networkx as nx
from pacman.msg import actions
from pacman.msg import pacmanPos
from pacman.msg import ghostsPos
from pacman.msg import cookiesPos
from pacman.msg import bonusPos
from pacman.msg import game
from pacman.msg import performance
from pacman.srv import mapService

#Posicion de PacMan
PosPacmanX=None
PosPacmanY=0
galletas=None
bonus=None
N=0
M=0
def pacmanPosCallback(msg):
    #rospy.loginfo('# Pacmans: {} posX: {} PosY: {}'.format(msg.nPacman, msg.pacmanPos.x, msg.pacmanPos.y) )
    global PosPacmanX
    PosPacmanX=msg.pacmanPos.x
    global PosPacmanY
    PosPacmanY=msg.pacmanPos.y

def ghostsPosCallback(msg):
#    rospy.loginfo('# Ghosts: {} '.format(msg.nGhosts)) 
#    for i in range(msg.nGhosts):
#        rospy.loginfo('Pos Ghosts {}: PosX {} PosY {}'.format(i, msg.ghostsPos[i].x, msg.ghostsPos[i].y))
    pass

def cookiesPosCallback(msg):
#    rospy.loginfo('# Cookies: {} '.format(msg.nCookies))
    global N	
    N=msg.nCookies
    global galletas
    galletas=msg.cookiesPos
    
def bonusPosCallback(msg):
    global M	
    M=msg.nBonus
    global bonus
    bonus=msg.bonusPos
#    rospy.loginfo('# bonus: {} '.format(msg.nBonus)) 
#    for i in range(msg.nBonus):
#        rospy.loginfo('Pos Bonus {}: PosX {} PosY {}'.format(i, msg.bonusPos[i].x, msg.bonusPos[i].y))
   

def gameStateCallback(msg):
#    rospy.loginfo('Game State: {} '.format(msg.state)) 
    pass

def performanceCallback(msg):
#    rospy.loginfo('Lives: {} Score: {} Time: {} PerformEval: {}'.format(msg.lives, msg.score, msg.gtime, msg.performEval) )
    pass


def pacman_controller_py():
    rospy.init_node('pacman_controller_py', anonymous=True)
    pub = rospy.Publisher('pacmanActions0', actions, queue_size=10000)
    rospy.Subscriber('pacmanCoord0', pacmanPos, pacmanPosCallback)
    rospy.Subscriber('ghostsCoord', ghostsPos, ghostsPosCallback)
    rospy.Subscriber('cookiesCoord', cookiesPos, cookiesPosCallback)
    rospy.Subscriber('bonusCoord', bonusPos, bonusPosCallback)
    rospy.Subscriber('gameState', game, gameStateCallback)
    rospy.Subscriber('performanceEval', performance, performanceCallback)


    try:
        #Llamar mapa
        mapRequestClient = rospy.ServiceProxy('pacman_world', mapService)
        mapa = mapRequestClient("Controller py")
        inicio=True

        #Obtener cosas en el mapa
 	while (galletas==None or PosPacmanX==None):
		pass
        Pos_Cookies=list()
        Pos_Bonus=list()
        for i in range(N):
            Pos_Cookies.append([galletas[i].x,galletas[i].y])
        for i in range(M):
            Pos_Bonus.append([bonus[i].x,bonus[i].y])
                
        #Max rate
        rate = rospy.Rate(6.66666666666666666666666666666666666666666666666666) # 10hz

        #Generar grafo
        z=np.array(mapa.obs)
        G=nx.DiGraph()
        m=0
        for i in range(mapa.minY,mapa.maxY+1):
            for j in range(mapa.minX,mapa.maxX+1):
                r=True
                for h in z:
                    if(i==h.y and j==h.x):
                        r=False
                        break
                if(r):
                    c = False
                    l = "vacio"
                    for h in Pos_Cookies:
                    	if(i==h[1] and j==h[0]):
                           c=True
                           l = "galleta"
                           break
                    if(c==False):
                      for h in Pos_Bonus:
                        if(i==h[1] and j==h[0]):
                            c=True
                            l = "bono"
                            break
                    G.add_node(m,x=j,y=i,z=l)
                    m+=1

        #Generar arcos con pesos
        for w in G.nodes(data=True):
            m=w[0]
            i=w[1]['x']
            j=w[1]['y']
            k=[x for x, y in G.nodes(data=True) if (y['x']==i and y['y']==j-1)or(y['x']==i-1 and y['y']==j)or(y['x']==i+1 and y['y']==j)or(y['x']==i and y['y']==j+1)]
            for u in k:
                g=100
                if G.node[u]['z']=="galleta":
                    g=20
                if G.node[u]['z']=="bono":
                    g=0
                G.add_edge(m,u,weight=g)

        #nx.draw(G,with_labels=True)
        #plt.show()
        msg = actions()

        comandos = list()
        cam = list()
        while not rospy.is_shutdown():
            #Asignar un nodo a PacMan
            p=[x for x, y in G.nodes(data=True) if (y['x']==PosPacmanX and y['y']==PosPacmanY)]
	    #print('posicion pacman',G.nodes[p[0]],PosPacmanX,PosPacmanY)
            if(len(comandos)<1):
                caminos = nx.single_source_dijkstra_path(G,p[0])
                f = 0
                a = 100000
                for j in caminos.keys():
                    b = G.nodes[j]
                    if b['z']!="vacio" and len(caminos[j])<a:
                        a = len(caminos[j])
                        f = j
                cam=caminos[f]
                for i in range(1,len(cam)):
                    k = G.nodes[cam[i-1]]
                    k1 = G.nodes[cam[i]]
                    if(k['x']==k1['x']):
                        if(k['y']>k1['y']):
                            #mover abajo
                            comandos.append(1)
                        else:
                            #mover arriba
                            comandos.append(0)
                    elif(k['x']>k1['x']):
                    
                    #mover a la izquierda
                        comandos.append(3)
                    else:
                        #mover a la derecha
                        comandos.append(2)
                cam.pop(0)
		if(inicio):
			msg.action = 4
			pub.publish(msg.action)
			inicio=False	
		#print(comandos)
            #escogemos el comando
            r = comandos.pop(0)
            n = cam.pop(0)
            for d in G.neighbors(n):
                G.add_edge(d,n,weight=100)
            G.nodes[n]["z"]="vacio"
            msg.action = r
            #pacmanPosCallback(pacmanPos())
            #print(msg.action)
            pub.publish(msg.action)
            rate.sleep()
		
            


        
    except rospy.ServiceException as e:
        print ("Error!! Make sure pacman_world node is running ")
    
if __name__ == '__main__':
    try:
        pacman_controller_py()
    except rospy.ROSInterruptException:
        pass


