import numpy as np
import networkx as nx
import matplotlib.pyplot as plt
import random

#Crear grafo
G=nx.DiGraph()
'''
#Crear nodos
n=0
for i in range(5):
    for j in range(5):
        p = np.random.randint(3)
        if p==0:
            continue
        else:
            if p==1:
                G.add_node(n, x=i,y=j,z=p)
            else:
                G.add_node(n, x=i,y=j,z=p)
            n+=1

#Crear arcos entre vecinos
for o in G.nodes(data=True):
    n = o[0]
    i = o[1]["x"]
    j = o[1]["y"]
    k = [x for x,y in G.nodes(data=True) if (y['x']==i and y['y']==j-1)or(y['x']==i-1 and y['y']==j)]
    for u in k:
        G.add_edge(n, u, weight=G.node[0]['z'])

#Camino mas corto desde un nodo
print(nx.single_source_dijkstra_path(G,0))

#Imprimir nodos
print(G.nodes(data=True))
'''
n=0
for i in range(3):
    for j in range(3):
        G.add_node(n,x=i,y=j,z=random.choice(["galleta","vacio","bono"]))
        n+=1
for w in G.nodes(data=True):
    m=w[0]
    i=w[1]['x']
    j=w[1]['y']
    k=[x for x, y in G.nodes(data=True) if (y['x']==i and y['y']==j-1)or(y['x']==i-1 and y['y']==j)or(y['x']==i+1 and y['y']==j)or(y['x']==i and y['y']==j+1)]
    if m == 0:
        print( k)
    for u in k:
        g=100
        if G.node[u]['z']=="galleta":
            g=20
        if G.node[u]['z']=="bono":
            g=0
        G.add_edge(m,u,weight=g)

p=[x for x, y in G.nodes(data=True) if (y['x']==0 and y['y']==0)]
print(nx.single_source_dijkstra_path(G,p[0]))
print(G.node[p[0]])
for w in G.nodes(data=True):
    print (w)
for w in G.edges(data=True):
    print (w)
#Graficar grafo
nx.draw(G, with_labels=True, font_weight='bold', pos = nx.spring_layout(G))
plt.show()
