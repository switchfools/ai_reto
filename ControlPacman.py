import sys
import glob
import serial
import time

import sys
import glob
import serial


def serial_ports():
    """ Lists serial port names
        
        :raises EnvironmentError:
        On unsupported or unknown platforms
        :returns:
        A list of the serial ports available on the system
        """
    if sys.platform.startswith('win'):
        ports = ['COM%s' % (i + 1) for i in range(256)]
    elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
        # this excludes your current terminal "/dev/tty"
        ports = glob.glob('/dev/tty[A-Za-z]*')
    elif sys.platform.startswith('darwin'):
        ports = glob.glob('/dev/tty.usbmodem*')
    else:
        raise EnvironmentError('Unsupported platform')
    
    result = []
    for port in ports:
        try:
            s = serial.Serial(port)
            s.close()
            result.append(port)
        except (OSError, serial.SerialException):
            pass
    return result


if __name__ == '__main__':
    print(serial_ports())


if __name__ == '__main__':
	try:
		ports=serial_ports()
		Baud=38400
		arduino = serial.Serial(ports[0], baudrate=Baud, timeout=1.0)
		# Nota: provocamos un reseteo manual de la placa para leer desde
		# el principio, ver http://stackoverflow.com/a/21082531/554319
		arduino.setDTR(False)
		time.sleep(1)
		arduino.flushInput()
		arduino.setDTR(True)
	except (ImportError, serial.SerialException):
    # No hay modulo serial o placa Arduino disponibles
	    import io
	    
	    class FakeArduino(io.RawIOBase):
	        """Clase para representar un "falso Arduino"
	            """
	        def readline(self):
	            time.sleep(0.1)
	            return b'sensor = 0\toutput = 0\r\n'
	    
	    arduino = FakeArduino()
with arduino:
    Data=list
    while True:
        try:
            # En Python 3 esta funcion devuelve un objeto bytes, ver
            # http://docs.python.org/3/library/stdtypes.html#typebytes
            line = arduino.readline()
            # Con errors='replace' se evitan problemas con bytes erroneos, ver
            # http://docs.python.org/3/library/stdtypes.html#bytes.decode
            # Con end='' se evita un doble salto de linea
            linedata=line.decode('ascii', errors='replace')
            #Data.append(linedata)
            print(linedata)
            #time.sleep(0.15)
        except KeyboardInterrupt:
            print("Exiting")
            break        
    #Berlin=pd.DataFrame(Data)
    #Berlin.to_csv('respuesta_escalon.txt',encoding='utf-8', index=False)
